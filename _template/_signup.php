<?php
$access= false;
$result=false;

if (isset($_POST['username']) and isset($_POST['password']) and isset($_POST['phone']) and isset($_POST['email_address'])) {
    $username=$_POST['username'];
    $password=md5($_POST['password']);
    $phone=$_POST['phone'];
    $email=$_POST['email_address'];
    $access=true;

    $result=User::signup($username, $email, $phone, $password);
    // print_r($_POST);
}

if ($access) {
    if ($result) {
        ?>
<main class="px-3">
	<h1>signup successful</h1>
	<p class="lead">Now you can login from <a href="/login.php">here</a>.</p>
	</p>
</main>
<?php	} else {?>
<main class="px-3">
	<h1>signup fail</h1>
	<p class="lead">Cover is a one-page template for building simple and beautiful home pages. Download, edit the text,
		and add your own fullscreen background photo to make it your own.</p>
	<p class="lead">
		<a href="#" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Learn more</a>
	</p>

	<?}
} else {?>


	<main class="form-signup">
		<form method="post" action="signup.php">
			<img class="mb-4" src="https://git.selfmade.ninja/uploads/-/system/appearance/logo/1/Logo_Dark.png" alt=""
				height="50">
			<h1 class="h3 mb-3 fw-normal">Please sign up here</h1>

			<div class="form-floating">
				<input name="username" type="text" class="form-control" id="floatingInput"
					placeholder="name@example.com">
				<label for="floatingInput">USERNAME</label>
			</div>
			<div class="form-floating">
				<input name="phone" type="phone" class="form-control" id="floatingInput" placeholder="name@example.com">
				<label for="floatingInput">PHONE</label>
			</div>
			<div class="form-floating">
				<input name="email_address" type="email" class="form-control" id="floatingInput"
					placeholder="name@example.com">
				<label for="floatingInput">Email address</label>
			</div>
			<div class="form-floating">
				<input name="password" type="password" class="form-control" id="floatingPassword"
					placeholder="Password">
				<label for="floatingPassword">Password</label>
			</div>

			<div class="checkbox mb-3">
				<label>
					<input type="checkbox" value="remember-me"> Remember me
				</label>
			</div>
			<button class="w-100 btn btn-lg btn-primary hvr-grow-rotate" type="submit">Sign in</button>
		</form>
	</main>
	<?}?>