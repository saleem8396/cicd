<?php
include 'lib/load.php'
?>
<!doctype html>
<html lang="en">
<?php load_templates("_head");?>

<body>

	<?php load_templates("_header");?>

	<main>

		<?php load_templates('_body');?>


		<?php load_templates('_calltofunction'); ?>

	</main>

	<?php load_templates("_footer");?>

	<script src="/assets/dist/js/bootstrap.bundle.min.js"></script>


</body>

</html>