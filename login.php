<?php
include 'lib/load.php'
?>

<!doctype html>
<html lang="en">

<head>


	<?php load_templates("_head");?>


	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}


		.form-signin {
			width: 100%;
			max-width: 330px;
			margin: auto;
			padding: 15px;
		}

		.form-signin .checkbox {
			font-weight: 400;
		}


		.form-signin .form-floating:focus-within {
			z-index: 2;
		}

		.form-signin input[type="email"] {
			margin-bottom: -1px;
			border-bottom-right-radius: 0;
			border-bottom-left-radius: 0;
		}

		.form-signin input[type="password"] {
			margin-bottom: 10px;
			border-top-left-radius: 0;
			border-top-right-radius: 0;
		}
	</style>


</head>

<body>

	<?php load_templates("_header");?>

	<main>
		<?php load_templates("_login");?>

	</main>

	<?php load_templates("_footer");?>

	<script src="/assets/dist/js/bootstrap.bundle.min.js"></script>


</body>

</html>